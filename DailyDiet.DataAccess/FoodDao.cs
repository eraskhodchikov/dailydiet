﻿using System;
using System.Collections.Generic;
using System.Linq;
using DailyDiet.Domain;

namespace DailyDiet.DataAccess
{
    public interface IFoodDao
    {
        IList<Food> GetFoodItems(Guid userId);

        Food GetFood(Guid userId, int id);
    }

    public class FoodDao : IFoodDao
    {
        private readonly IList<Food> _data = new List<Food>
        {
            new Food
            {
                Id = 1,
                Name = "Food 1",
                Components = new[]
                {
                    new FoodComponent {Id = 1, Name = "Protein", Quantity = 100},
                    new FoodComponent {Id = 2, Name = "Triglyceride", Quantity = 200},
                }
            },
            new Food
            {
                Id = 2,
                Name = "Food 2",
                Components = new[]
                {
                    new FoodComponent {Id = 1, Name = "Protein", Quantity = 150},
                    new FoodComponent {Id = 2, Name = "Triglyceride", Quantity = 100},
                }
            },
        };

        public IList<Food> GetFoodItems(Guid userId)
        {
            return _data;
        }

        public Food GetFood(Guid userId, int id)
        {
            return _data.FirstOrDefault(f => f.Id == id);
        }
    }
}
