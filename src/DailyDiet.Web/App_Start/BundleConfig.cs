﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using Newtonsoft.Json;

namespace DailyDiet.Web
{
    public class BundleConfig
    {
        private const string BundleJsonVirtualPath = "~/App_Data/bundles.json";

        public static void RegisterBundles(BundleCollection bundles)
        {
            var bundlesJsonFilePath = HttpContext.Current.Server.MapPath(BundleJsonVirtualPath);
            var bundlesJsonContent = ReadFileContent(bundlesJsonFilePath);

            var bundlesSettings = JsonConvert.DeserializeObject<Dictionary<string, BundleSettings>>(bundlesJsonContent);

            foreach (var bundleContent in  bundlesSettings)
            {
                foreach (var fileType in Enum.GetValues(typeof (FileType)).OfType<FileType>())
                {
                    var paths = GetPaths(bundlesSettings, bundleContent.Key, fileType).ToArray();
                    if (paths.Length <= 0)
                    {
                        continue;
                    }

                    var virtualPath = GetVirtualPath(bundleContent.Key, fileType);
                    var bundle = BundleFactory.Create(fileType, virtualPath);
                    bundle.Include(paths);
                    BundleTable.Bundles.Add(bundle);
                }
            }
        }

        public static IHtmlString RenderScripts(string bundleName)
        {
            return Render(bundleName, ContentType.Script);
        }

        public static IHtmlString RenderStyles(string bundleName)
        {
            return Render(bundleName, ContentType.Style);
        }

        private static string ReadFileContent(string path)
        {
            using (var streamReader = new StreamReader(path))
            {
                return streamReader.ReadToEnd();
            }
        }

        private static IEnumerable<string> GetPaths(IReadOnlyDictionary<string, BundleSettings> bundlesSettings, string bundleName,
            FileType fileType)
        {
            if (!bundlesSettings.ContainsKey(bundleName))
            {
                yield break;
            }

            var bundle = bundlesSettings[bundleName];

            if (bundle.Dependencies != null)
            {
                foreach (var path in bundle.Dependencies.SelectMany(d => GetPaths(bundlesSettings, d, fileType)))
                {
                    yield return path;
                }
            }

            if (bundle.Files != null && bundle.Files.ContainsKey(fileType))
            {
                foreach (var file in bundle.Files[fileType])
                {
                    yield return file;
                }
            }
        }

        private static string GetVirtualPath(string name, FileType fileType)
        {
            return $"{"~/bundles/"}{name}/{fileType}";
        }

        public class BundleSettings
        {
            [JsonProperty("d")]
            public string[] Dependencies { get; set; }

            [JsonProperty("f")]
            public Dictionary<FileType, string[]> Files { get; set; }
        }

        private static class BundleFactory
        {
            private static readonly Dictionary<FileType, Func<string, Bundle>> Bundles =
                new Dictionary<FileType, Func<string, Bundle>>
                {
                    [FileType.Js] = virtualPath => new ScriptBundle(virtualPath),
                    [FileType.Css] = virtualPath => new CssBundle(virtualPath),
                    [FileType.Less] = virtualPath => new LessBundle(virtualPath)
                };

            public static Bundle Create(FileType fileType, string virtualPath)
            {
                return Bundles[fileType](virtualPath);
            }
        }

        private class CssBundle : StyleBundle
        {
            public CssBundle(string virtualPath) : base(virtualPath)
            {
            }

            public override Bundle Include(params string[] virtualPaths)
            {
                foreach (var virtualPath in virtualPaths)
                {
                    Include(virtualPath, new CssRewriteUrlTransform());
                }
                return this;
            }
        }

        private class LessBundle : Bundle
        {
            public LessBundle(string virtualPath)
                : base(
                    virtualPath,
                    new LessTransform(),
                    new CssMinify())
            {
            }

            public override Bundle Include(params string[] virtualPaths)
            {
                foreach (var virtualPath in virtualPaths)
                {
                    Include(virtualPath, new CssRewriteUrlTransform());
                }
                return this;
            }
        }

        private class LessTransform : IBundleTransform
        {
            private const string ContentType = "text/css";

            public void Process(BundleContext context, BundleResponse response)
            {
                response.Content = dotless.Core.Less.Parse(response.Content);
                response.ContentType = ContentType;
            }
        }

        private static IHtmlString Render(string name, ContentType contentType)
        {
            var renderFunc = RenderFuncFactory.Get(contentType);
            var fileTypes = contentType.GetFileTypes();

            var bundles = fileTypes
                .Select(f => GetVirtualPath(name, f))
                .Where(f => BundleTable.Bundles.Any(b => b.Path == f))
                .ToArray();

            return bundles.Length != 0
                ? new HtmlString(string.Join(string.Empty, renderFunc(bundles)))
                : new HtmlString(string.Empty);
        }

        private static class RenderFuncFactory
        {
            private static readonly Dictionary<ContentType, Func<string[], IHtmlString>> RenderFuncs =
                new Dictionary<ContentType, Func<string[], IHtmlString>>
                {
                    [ContentType.Script] = Scripts.Render,
                    [ContentType.Style] = Styles.Render
                };

            public static Func<string[], IHtmlString> Get(ContentType contentType)
            {
                return RenderFuncs[contentType];
            }
        }
    }

    public enum FileType
    {
        Js,
        Css,
        Less
    }

    public enum ContentType
    {
        Script,
        Style
    }

    internal static class ContentTypeExtension
    {
        private static readonly Dictionary<ContentType, FileType[]> FileTypes =
            new Dictionary<ContentType, FileType[]>
            {
                [ContentType.Script] = new[] { FileType.Js },
                [ContentType.Style] = new[] { FileType.Css, FileType.Less }
            };

        public static FileType[] GetFileTypes(this ContentType contentType)
        {
            return FileTypes[contentType];
        }
    }
}
