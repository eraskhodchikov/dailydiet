﻿using System.Web.Http;

namespace DailyDiet.Web
{
    public static class WebApiConfig
    {
        public static string UrlPrefix => "api";
        public static string UrlPrefixRelative => "~/" + UrlPrefix;

        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "DefaultApi",
                UrlPrefix + "/{controller}/{id}",
                new {id = RouteParameter.Optional});
        }
    }
}
