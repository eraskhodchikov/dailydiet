﻿using System;
using System.Security.Principal;
using Microsoft.AspNet.Identity;

namespace DailyDiet.Web.Models
{
    public static class IdentityExtension
    {
        public static Guid UserId(this IIdentity identity)
        {
            return Guid.Parse(identity.GetUserId());
        }
    }
}