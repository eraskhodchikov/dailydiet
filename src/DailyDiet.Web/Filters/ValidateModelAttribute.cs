﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace DailyDiet.Web.Filters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        private const HttpStatusCode BadRequest = HttpStatusCode.BadRequest;
        private const string ModelIsNullMessage = "Arguments cannot be null";

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var request = actionContext.Request;

            if (ModelIsNull(actionContext))
            {
                actionContext.Response = request.CreateErrorResponse(BadRequest, ModelIsNullMessage);
            }

            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = request.CreateErrorResponse(BadRequest, actionContext.ModelState);
            }
        }

        private static bool ModelIsNull(HttpActionContext actionContext)
        {
            return actionContext.ActionArguments.Any(kv => kv.Value == null);
        }
    }
}