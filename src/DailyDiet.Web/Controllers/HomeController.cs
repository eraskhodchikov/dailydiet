﻿using System.Web.Mvc;

namespace DailyDiet.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}