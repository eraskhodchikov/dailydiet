﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Description;
using DailyDiet.DataAccess;
using DailyDiet.Web.Filters;
using DailyDiet.Web.Models;
using Microsoft.AspNet.Identity;

namespace DailyDiet.Web.Controllers.api
{
    /// <summary>
    /// Food REST controller.
    /// </summary>
    [Authorize]
    [RoutePrefix("api/food")]
    public class FoodController : ApiController
    {
        private const string IdParameter = "{id:int:min(1)}";

        private readonly IFoodDao _foodDao;

        public FoodController()
            : this(new FoodDao())
        {
        }

        public FoodController(IFoodDao foodDao)
        {
            _foodDao = foodDao;
        }

        /// <summary>
        /// Get all food items.
        /// </summary>
        [Route("")]
        public IEnumerable<FoodDto> GetFoodList()
        {
            Thread.Sleep(500);
            return _foodDao.GetFoodItems(User.Identity.UserId())
                .Select(f => new FoodDto()
                {
                    Id = f.Id,
                    Name = f.Name
                });
        }

        /// <summary>
        /// Get single food item by id.
        /// </summary>
        /// <param name="id">Unique food identifier</param>
        [Route(IdParameter, Name = nameof(GetFood))]
        [ResponseType(typeof (FoodDto))]
        public IHttpActionResult GetFood(int id)
        {
            Thread.Sleep(500);
            return Ok(new FoodDto {Id = id, Name = "F" + id});
        }

        /// <summary>
        /// Create new food item.
        /// </summary>
        /// <param name="food">Food item.</param>
        [Route("")]
        [ResponseType(typeof (FoodDto))]
        [ValidateModel]
        public IHttpActionResult PostFood(FoodDto food)
        {
            food.Id = 1;

            return CreatedAtRoute(nameof(GetFood), new {id = food.Id}, food);
        }

        /// <summary>
        /// Update existing food item.
        /// </summary>
        /// <param name="id">Food item identifier.</param>
        /// <param name="food">Food item</param>
        [Route(IdParameter)]
        [ResponseType(typeof (void))]
        [ValidateModel]
        public IHttpActionResult PutFood(int id, FoodDto food)
        {
            if (id != food.Id)
            {
                return BadRequest();
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Delete existing food item.
        /// </summary>
        [Route(IdParameter)]
        [ResponseType(typeof (int))]
        public IHttpActionResult DeleteFood(int id)
        {
            return Ok(id);
        }
    }

    /// <summary>
    /// Food item.
    /// </summary>
    public class FoodDto
    {
        /// <summary>
        /// Unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Food name.
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
