﻿namespace DailyDiet.Domain
{
    public class DishFood
    {
        public int Weight { get; set; }

        public Food Food { get; set; }

        public int SortNum { get; set; }
    }
}