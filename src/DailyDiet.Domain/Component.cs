﻿namespace DailyDiet.Domain
{
    public class Component
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public UnitOfMeasurement UnitOfMeasurement { get; set; }
    }
}