﻿namespace DailyDiet.Domain
{
    public enum UnitOfMeasurement
    {
        Gram,
        Milligram
    }
}