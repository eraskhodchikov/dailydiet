﻿namespace DailyDiet.Domain
{
    public class Dish
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DishFood[] FoodItems { get; set; }
    }
}