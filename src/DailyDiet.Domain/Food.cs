﻿namespace DailyDiet.Domain
{
    public class Food
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public FoodComponent[] Components { get; set; }
    }
}